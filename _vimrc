call plug#begin('~/vimfiles/plugged')


" Theme
Plug 'challenger-deep-theme/vim', { 'as': 'challenger-deep' }
" File Explorer
Plug 'scrooloose/nerdtree'
" Show diff in gutter
Plug 'airblade/vim-gitgutter'
" Status bar on bottom
Plug 'itchyny/lightline.vim'
" Project quick search
Plug 'ctrlpvim/ctrlp.vim'
" syntax support
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'mxw/vim-jsx'
Plug 'groenewege/vim-less'
Plug 'tpope/vim-markdown'

" Changes Vim working directory to project root
Plug 'airblade/vim-rooter'
" Some sane defaults
Plug 'tpope/vim-sensible'
" Git integration
Plug 'tpope/vim-fugitive'
" Surround commands
Plug 'tpope/vim-surround'
" Smart indentation
Plug 'tpope/vim-sleuth'
" Async commands
Plug 'w0rp/ale'
Plug 'skywind3000/asyncrun.vim'



" Initialize plugin system
call plug#end()


if has('nvim') || has('termguicolors')
  set termguicolors
endif

colorscheme challenger_deep
let g:lightline = {
      \ 'colorscheme': 'challenger_deep',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

map <C-n> :NERDTreeToggle<CR>

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|target'

set relativenumber

set guifont=Consolas

" Handled by vim-sensible
" set backspace=indent,eol,start

 set noswapfile

let g:ale_sign_error = '●' " Less aggressive than the default '>>'
let g:ale_sign_warning = '.'
let g:ale_lint_on_enter = 0 " Less distracting when opening a new file

" Disabling becase we added the pre-commit linting
" autocmd BufWritePost *.js AsyncRun -post=checktime ./node_modules/.bin/eslint --fix %
"

" remap terminal exit
tnoremap <Esc> <C-w>N